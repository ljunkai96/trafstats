import streamlit as st
import streamlit.components.v1 as components
import os
import pandas as pd
import matplotlib.pyplot as plt
from PIL import Image


# html layouts
placeholder_box = """
<p>The rate of accident for the selected travel details:</p>
<div style="background-color:light blue; width:50px; height:50px; border:3px solid black;">
<span><b>3.7%</b></span>
</div>
"""


def checkPlatform():  # Check if running on local or heroku
    if(os.path.exists(os.path.join(os.getcwd(), "data"))):
        path = "data/"
    else:
        path = "app/data/"

    return path


def topCombination(df):
    conditions = ['Speed_limit', 'Road_Type', 'Light_Conditions',
                  'Weather_Conditions', 'Road_Surface_Conditions']
    df_combi = df[conditions]
    st.write("## The 10 Combinations of Conditions That Was Present During Accidents")
    st.write("""
        There is a total number of **466239** traffic accidents that occur with 
        these combination of conditions...

        * Speed Limit: 30
        * Road Type: Single Carriageway
        * Light Conditions: Daylight - Street Lights Present
        * Weather Conditions: Fine Without High Winds
        * Road Surface Conditions: Dry        
    """)
    st.write(df_combi.groupby(conditions).size().reset_index(
        name='Count').sort_values('Count', ascending=False)[:10])


def home(df):

    st.write("# Welcome to TrafStats!")
    # Get yearly data
    st.write("""
        This is a data analytical and machine learning website that allows users to 
        analyse and predict the causes of traffic accidents in the United Kingdom. 
        This website is useful for government agencies, transport and development 
        authorities, and other businesses who wishes to plan and make informed 
        decisions to improve on their future travel.
    """)
    # st.write("***")
    st.write("## **Brief Overview**")
    st.write("""
        Below is an overview of some of the analysis done by the TrafStats team, to 
        bring out the most valuable insights regarding the causes of traffic accidents 
        in the United Kingdom, for the year of **2009 - 2019**. An approximately 1.6 million 
        rows of data are preprocessed and analysed for the machine learning prediction. Some 
        of the features includes...
        
        * Speed Limit   
        * Road Type    
        * Light Conditions   
        * Weather Conditions    
        * Road Surface Conditions
    """)
    st.write(
        "## Total Number of Accidents Reported Over the Years")
    yearly_df = df["Year"].value_counts(
        sort=False).rename_axis('Year').to_frame('Counts')

    with st.beta_container():
        col1, col2 = st.beta_columns([2, 1])
        with col1:
            # Matplotlib line chart
            fig, ax = plt.subplots()
            ax.plot(yearly_df)
            st.pyplot(fig)

        with col2:
            st.write(yearly_df)

        col3, col4 = st.beta_columns([1, 1])
        path = checkPlatform()
        with col3:
            st.write(
                "## Accidents Rate for Each Speed Zone")
            speedpie = Image.open(path + "speedpie.png")
            st.image(speedpie, use_column_width=True)

        with col4:
            st.write(
                "## Accidents Rate for the Different Times of Day")
            timehist = Image.open(path + "timehist.png")
            st.image(timehist, use_column_width=True)

        st.write("_*Hover on the image and click to expand to view in fullsize_")
        st.write("***")
        topCombination(df)
        st.write("***")
        st.write("""
            # Navigation
            You can use the navigation panel on the left to navigate through different functions 
            of the website. You can analyse the UK Traffic Accidents dataset through visual 
            representations, as well as make your own predictions based on different machine 
            learning models. Finally, you can explore the dataset and even create your own dataset 
            for your own practice.
        """)
        st.write("***")

    # st.markdown("""
    # * You have not make any prediction
    # """)

    """
    
    my_component = components.declare_component(
        "my_component",
        url="http://localhost:8000"
        )

    components.iframe("http://localhost:8000", width=700, height=500)
    """
