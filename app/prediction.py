import streamlit as st
import numpy as np
import pandas as pd
import streamlit.components.v1 as components
import matplotlib.pyplot as plt
import os

#import seaborn as sns

import joblib

# html templates
bold_temp = """
	<b>{}</b>
	"""


def checkPlatform():  # Check if running on local or heroku
    if(os.path.exists(os.path.join(os.getcwd(), "models"))):
        path = "models/"
    else:
        path = "app/models/"

    return path


# Dictionary of Mapped Values
d_road_type = {'Dual carriageway': 0,
               'One way street': 1,
               'Roundabout': 2,
               'Single carriageway': 3,
               'Slip road': 4,
               'Unknown': 5}

d_light_conditions = {'Darkness: No street lighting': 0,
                      'Darkness: Street lighting unknown': 1,
                      'Darkness: Street lights present and lit': 2,
                      'Darkness: Street lights present but unlit': 3,
                      'Daylight: Street light present': 4}

d_weather_conditions = {'Fine with high winds': 0,
                        'Fine without high winds': 1,
                        'Fog or mist': 2,
                        'Other': 3,
                        'Raining with high winds': 4,
                        'Raining without high winds': 5,
                        'Snowing with high winds': 6,
                        'Snowing without high winds': 7,
                        'Unknown': 8}

d_road_surface_conditions = {'Dry': 0,
                             'Flood (Over 3cm of water)': 1,
                             'Frost/Ice': 2,
                             'Snow': 3,
                             'Wet/Damp': 4}

# convert selection to encoded array


def get_encoded_arr(selection, my_dict):
    num = get_value(selection, my_dict)
    length = len(my_dict)
    arr = []
    for i in range(length):
        if i == num:
            arr.append(1)
            continue
        arr.append(0)
    return arr

# Find the Value from the dictionary


def get_value(val, my_dict):
    for key, value in my_dict.items():
        if val == key:
            return value

# Find the Key from Dictionary


def get_key(val, my_dict):
    for key, value in my_dict.items():
        if val == value:
            return key


"""
# Load Models
def load_model_n_predict(model_file):
	loaded_model = pickle.load(open(model_file, "rb"))
	return loaded_model

"""
# Load Models


def load_model_n_predict(model_file):
    loaded_model = joblib.load(open(model_file, "rb"))
    return loaded_model


def prediction():

    # get os path
    path = checkPlatform()

    # This is the header and description section
    st.header("Make your predictions here! :bulb:")
    st.markdown("""
        _There are many conditions that are present during a traffic accident.
        Use our machine learning models to help predict which traffic environment
        conditions that may contribute to an accident._
    """)

    st.markdown("**Steps to Create Your Own Predictions**")
    st.markdown("""
        1. Fill in the form (You can leave some of the fields blank)
        2. Choose what you want to predict 
        3. Choose the model to use for the prediction
        4. Happy predicting!
    """)

    # These are the form options
    road_type = ['Dual carriageway',
                 'One way street',
                 'Slip road',
                 'Roundabout',
                 'Unknown',
                 'Single carriageway']

    light_conditions = ['Daylight: Street light present',
                        'Darkness: No street lighting',
                        'Darkness: Street lights present and lit',
                        'Darkness: Street lights present but unlit',
                        'Darkness: Street lighting unknown']

    weather_conditions = ['Fog or mist',
                          'Snowing with high winds',
                          'Raining without high winds',
                          'Raining with high winds',
                          'Snowing without high winds',
                          'Fine without high winds',
                          'Fine with high winds',
                          'Unknown',
                          'Other']

    road_surface_conditions = ['Dry',
                               'Snow',
                               'Wet/Damp',
                               'Frost/Ice',
                               'Flood (Over 3cm of water)']

    """ Construction of form """

    st.markdown("**Vehicles**")
    input_veh = st.number_input("Number of Vehicles", value=0, min_value=0)
    input_speed = st.number_input("Speed of Vehicles", value=0, min_value=0)
    input_severity = st.number_input(
        "Estimated Severity of Accident", value=1, min_value=1, max_value=3)

    st.markdown("**Date**")
    c1, c2, c3 = st.beta_columns(3)
    c4, c5 = st.beta_columns(2)
    input_day = c1.number_input("Day", value=1, min_value=1, max_value=31)
    input_month = c2.number_input("Month", value=1, min_value=1, max_value=12)
    input_year = c3.number_input("Year", value=2020, max_value=2030)

    st.markdown("**Conditions**")
    roadtype = st.radio("Road Type", road_type)
    lightcondition = st.radio("Light Condition", light_conditions)
    weathercondition = st.radio("Weather Condition", weather_conditions)
    roadsurfacecondition = st.radio(
        "Road Surface Condition", road_surface_conditions)

    # Process user input
    # GET VALUES FOR EACH INPUT
    arr_roadtype = get_encoded_arr(roadtype, d_road_type)
    arr_lightcondition = get_encoded_arr(lightcondition, d_light_conditions)
    arr_weathercondition = get_encoded_arr(
        weathercondition, d_weather_conditions)
    arr_roadsurfacecondition = get_encoded_arr(
        roadsurfacecondition, d_road_surface_conditions)

    st.markdown("**Prediction**")
    c6, c7 = st.beta_columns(2)
    choice_pred = c6.selectbox("What do you wish to predict?", [
                               'Accident Severity', 'Speed', 'Road Type', 'Light Condition', 'Weather Condition', 'Road Surface Condition'])
    choice_model = c7.selectbox(
        "Choice of model", ['Decision Tree', 'Naive Bayes'])

    if st.button("Predict"):
        if choice_pred == "Accident Severity":
            """ Accident Severity input concat """
            # input params ['Number_of_Vehicles', 'Day', 'Month, 'Year', 'Speed', Road_Type', 'Light_Conditions', 'Weather_Conditions','Road_Surface_Conditions']
            temp_list = []
            temp_list.append(input_veh)
            temp_list.append(input_day)
            temp_list.append(input_month)
            temp_list.append(input_year)
            temp_list.append(input_speed)

            edited_input = temp_list + arr_roadtype + arr_lightcondition + \
                arr_weathercondition + arr_roadsurfacecondition
            reshaped_input = np.array(edited_input).reshape(1, -1)

            if choice_model == "Decision Tree":
                model = load_model_n_predict(path + "dtree_severity")
                prediction = model.predict(reshaped_input)
                st.markdown("**The predicted Accident Severity with the given inputs is: **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)
                st.markdown("""
                    _Accident Severity refers to how severe an accident is. 
                    The different combinations of road conditions will result in
                    different severity of the accident ranging from 1 - 3. 
                    **1** being the least severe, **3** being the most severe._
                """)

            if choice_model == "Naive Bayes":
                model = load_model_n_predict(path + "gnb_severity")
                prediction = model.predict(reshaped_input)
                st.markdown("**The predicted Accident Severity with the given inputs is: **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)

                st.markdown("""
                    _Accident Severity refers to how severe an accident is. 
                    The different combinations of road conditions will result in
                    different severity of the accident ranging from 1 - 3. 
                    **1** being the least severe, **3** being the most severe._
                """)

            # if choice_model == "Linear Regression":
            #     model = load_model_n_predict(path + "lr_severity")
            #     prediction = model.predict(reshaped_input)

            #     st.markdown("**The predicted Accident Severity with the given inputs is: **" + bold_temp.format(prediction[0]), unsafe_allow_html=True)
            #     st.markdown("""
            #         _Accident Severity refers to how severe an accident is.
            #         The different combinations of road conditions will result in
            #         different severity of the accident ranging from 1 - 3.
            #         **1** being the least severe, **3** being the most severe._
            #     """)

        elif choice_pred == "Speed":
            """ Speed input concat """
            # input params ['Number_of_Vehicles', 'Day', 'Month, 'Year',
            # 'Severity', 'Road_Type', 'Light_Conditions',
            # 'Weather_Conditions','Road_Surface_Conditions']
            temp_list = []
            temp_list.append(input_veh)
            temp_list.append(input_day)
            temp_list.append(input_month)
            temp_list.append(input_year)
            temp_list.append(input_severity)

            edited_input = temp_list + arr_roadtype + arr_lightcondition + \
                arr_weathercondition + arr_roadsurfacecondition
            reshaped_input = np.array(edited_input).reshape(1, -1)

            if choice_model == "Decision Tree":
                model = load_model_n_predict(path + "dtree_speed")
                prediction = model.predict(reshaped_input)
                prediction[0] *= 10
                st.markdown("**The predicted Speed with the given inputs is: **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)
                st.markdown("""
                    _Speed refers to the maximum speed of the road when the accident occured. 
                    This will give us insights on the effect of road speeds and other conditions 
                    present during an accident._
                """)

            if choice_model == "Naive Bayes":
                model = load_model_n_predict(path + "gnb_speed")
                prediction = model.predict(reshaped_input)
                prediction[0] *= 10
                st.markdown("**The predicted Speed with the given inputs is: **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)
                st.markdown("""
                    _Speed refers to the maximum speed of the road when the accident occured. 
                    This will give us insights on the effect of road speeds and other conditions 
                    present during an accident._
                """)

        elif choice_pred == "Road Type":
            """ Speed input concat """
            # input params ['Number_of_Vehicles', 'Day', 'Month, 'Year', 'Speed', 'Severity', 'Light_Conditions', 'Weather_Conditions','Road_Surface_Conditions']
            temp_list = []
            temp_list.append(input_veh)
            temp_list.append(input_day)
            temp_list.append(input_month)
            temp_list.append(input_year)
            temp_list.append(input_speed)
            temp_list.append(input_severity)

            edited_input = temp_list + arr_lightcondition + \
                arr_weathercondition + arr_roadsurfacecondition
            reshaped_input = np.array(edited_input).reshape(1, -1)

            if choice_model == "Decision Tree":
                model = load_model_n_predict(path + "dtree_road")
                prediction = model.predict(reshaped_input)
                st.markdown("**The predicted Road Type with the given inputs is: **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)
                st.markdown("""
                    _With the given inputs, this prediction tells us which type of roads 
                    do accidents usually occur. Analyst can use this information to help 
                    provide a better understanding of the road conditions and to possibly 
                    contruct improved road structures or safety road signs and hazards._
                """)

            if choice_model == "Naive Bayes":
                model = load_model_n_predict(path + "gnb_road")
                prediction = model.predict(reshaped_input)
                st.markdown("**The predicted Road Type with the given inputs is: **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)
                st.markdown("""
                    _With the given inputs, this prediction tells us which type of roads 
                    do accidents usually occur. Analyst can use this information to help 
                    provide a better understanding of the road conditions and to possibly 
                    contruct improved road structures or safety road signs and hazards._
                """)

        elif choice_pred == "Light Condition":
            """ Speed input concat """
            # input params ['Number_of_Vehicles', 'Day', 'Month, 'Year', 'Speed', 'Severity', 'Road Type', 'Weather_Conditions','Road_Surface_Conditions']
            temp_list = []
            temp_list.append(input_veh)
            temp_list.append(input_day)
            temp_list.append(input_month)
            temp_list.append(input_year)
            temp_list.append(input_severity)
            temp_list.append(input_speed)

            edited_input = temp_list + arr_roadtype + \
                arr_weathercondition + arr_roadsurfacecondition
            reshaped_input = np.array(edited_input).reshape(1, -1)

            if choice_model == "Decision Tree":
                model = load_model_n_predict(path + "dtree_light")
                prediction = model.predict(reshaped_input)
                st.markdown("**The predicted Light Condition with the given inputs is **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)
                st.markdown("""
                    _This is the predicted light condition with the given inputs. Should the 
                    results predicted result have poor lighting conditions, motorists should
                    drive with more caution when travelling under such conditions. Additionally, 
                    more road lightings has to be implemented in that area._
                """)

            if choice_model == "Naive Bayes":
                model = load_model_n_predict(path + "gnb_light")
                prediction = model.predict(reshaped_input)
                st.markdown("**The predicted Light Condition with the given inputs is **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)
                st.markdown("""
                    _This is the predicted light condition with the given inputs. Should the 
                    results predicted result have poor lighting conditions, motorists should
                    drive with more caution when travelling under such conditions. Additionally, 
                    more road lightings has to be implemented in that area._
                """)
        elif choice_pred == "Weather Condition":
            """ Speed input concat """
            # input params ['Number_of_Vehicles', 'Day', 'Month, 'Year', 'Speed', 'Severity', 'Road Type', 'Weather_Conditions','Road_Surface_Conditions']
            temp_list = []
            temp_list.append(input_veh)
            temp_list.append(input_day)
            temp_list.append(input_month)
            temp_list.append(input_year)
            temp_list.append(input_severity)
            temp_list.append(input_speed)

            edited_input = temp_list + arr_roadtype + \
                arr_lightcondition + arr_roadsurfacecondition
            reshaped_input = np.array(edited_input).reshape(1, -1)

            if choice_model == "Decision Tree":
                model = load_model_n_predict(path + "dtree_weather")
                prediction = model.predict(reshaped_input)
                st.markdown("**The predicted Weather Condtion with the given inputs is: **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)
                st.markdown("""
                    _An accident would likely occur with this kind of weather 
                    condition, along with the combination of given inputs. Thus,
                    try to be careful when travelling under such weather._
                """)

            if choice_model == "Naive Bayes":
                model = load_model_n_predict(path + "gnb_weather")
                prediction = model.predict(reshaped_input)
                st.markdown("**The predicted Weather Condition with the given inputs is: **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)
                st.markdown("""
                    _An accident would likely occur with this kind of weather 
                    condition, along with the combination of given inputs. Thus,
                    try to be careful when travelling under such weather._
                """)

        elif choice_pred == "Road Surface Condition":
            """ Speed input concat """
            # input params ['Number_of_Vehicles', 'Day', 'Month, 'Year', 'Speed', 'Severity', 'Road Type', 'Weather_Conditions','Road_Surface_Conditions']
            temp_list = []
            temp_list.append(input_veh)
            temp_list.append(input_day)
            temp_list.append(input_month)
            temp_list.append(input_year)
            temp_list.append(input_severity)
            temp_list.append(input_speed)

            edited_input = temp_list + arr_roadtype + \
                arr_lightcondition + arr_weathercondition
            reshaped_input = np.array(edited_input).reshape(1, -1)

            if choice_model == "Decision Tree":
                model = load_model_n_predict(path + "dtree_surface")
                prediction = model.predict(reshaped_input)
                st.markdown("**The predicted Road Surface Condition with the given inputs is: **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)
                st.markdown("""
                    _Road surface condition plays a huge part in causing an accident. 
                    As it is hard to judge with naked eyes, it often catches drivers off guard. 
                    Try making a few predictions and be wary if you are driving with the 
                    given conditions._
                """)

            if choice_model == "Naive Bayes":
                model = load_model_n_predict(path + "gnb_surface")
                prediction = model.predict(
                    np.array(edited_input).reshape(-1, 1))
                st.markdown("**The predicted Road Surface Condition with the given inputs is: **" +
                            bold_temp.format(prediction[0]), unsafe_allow_html=True)
                st.markdown("""
                    _Road surface condition plays a huge part in causing an accident. 
                    As it is hard to judge with naked eyes, it often catches drivers off guard. 
                    Try making a few predictions and be wary if you are driving with the 
                    given conditions._
                """)
