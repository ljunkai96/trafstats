import streamlit as st
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import datetime
from PIL import Image
from pathlib import Path
import os
import pydeck as pdk


def checkPlatform():  # Check if running on local or heroku
    if(os.path.exists(os.path.join(os.getcwd(), "data"))):
        path = "data/"
    else:
        path = "app/data/"

    return path


def pydeck(df, year, month):

    df['Date'] = pd.to_datetime(df['Date'])
    df = df.loc[(df["Date"].dt.year == year) & (df["Date"].dt.month == month)]
    df = df.rename(columns={"Longitude": "lng", "Latitude": "lat"})
    st.write(df)

    st.pydeck_chart(pdk.Deck(
        map_style='mapbox://styles/mapbox/light-v9',
        initial_view_state=pdk.ViewState(
            latitude=51.5104,
            longitude=0.1301,
            zoom=8,
            pitch=10,
        ),
        layers=[
            pdk.Layer(
                'Accident_Severity',
                data=df,
                get_position='[lng, lat]',
                radius=200,
                elevation_scale=50,
                elevation_range=[0, 3000],
                pickable=True,
                extruded=True,
            ),
            pdk.Layer(
                'ScatterplotLayer',
                data=df,
                get_position='[lng, lat]',
                get_color='[200, 30, 0, 160]',
                get_radius=200,
            ),
        ]
    ))


def scatter_form():
    palette = st.selectbox(
        "Palette ", ['Paired', 'Accent', 'Purples', 'colorblind'])
    s = st.slider("Size per data point ", value=1, min_value=1, max_value=200)
    year = st.number_input("Year of data to be generated ",
                           value=2009, min_value=2009, max_value=2019)
    type = st.selectbox("Data to be plotted ", [
                        'Accident_Severity', 'Road_Type', 'Light_Conditions', 'Weather_Conditions', 'Road_Surface_Conditions'])
    return palette, s, year, type


def scatterplot(data, palette, s, year, type):

    data = data.loc[data['Year'] == year]

    fig, ax = plt.subplots(figsize=(15, 20))  # (x,y)
    sns.scatterplot(x='Longitude', y='Latitude',
                    data=data,
                    palette=palette,
                    hue=type,
                    s=s)

    st.pyplot(fig)


def categoricalplot(data, hue, palette):
    fig, ax = plt.subplots()
    fig = sns.catplot(x='Accident_Severity', hue=hue, data=data,
                      col='Road_Type', col_wrap=3, kind='count',
                      height=5, aspect=0.7, palette=palette,
                      margin_titles=True)
    fig.set_titles(row_template='{row_name}', col_template='{col_name}')
    st.pyplot(fig)


def lineplot(data, condition):
    data_plot = []
    temp_plot = []
    if condition == "Road Type":
        road_type = ['Dual carriageway',
                     'One way street',
                     'Slip road',
                     'Roundabout',
                     'Unknown',
                     'Single carriageway']

        for i in road_type:
            if temp_plot:
                data_plot.append(temp_plot)
                temp_plot = []
            for year in range(2009, 2020, 1):
                count = int(data[(data.Road_Type == i) & (
                    data.Year == year)].count()['Road_Type'])
                temp_plot.append(count)

        x_label = [2009, 2010, 2011, 2012, 2013,
                   2014, 2015, 2016, 2017, 2018, 2019]
        for j in range(len(data_plot)):
            plt.plot(x_label, data_plot[j])

        plt.title("Trend of the different " + condition + " over the years")
        plt.ylabel("Number of occurance")
        plt.xlabel("Year")
        plt.legend(road_type, title='title',
                   bbox_to_anchor=(1.05, 1), loc='upper left')
        st.pyplot(plt)

    if condition == "Light Condition":
        light_conditions = ['Darkness: No street lighting',
                            'Daylight: Street light present',
                            'Darkness: Street lights present and lit',
                            'Darkness: Street lights present but unlit',
                            'Darkness: Street lighting unknown']

        for i in light_conditions:
            if temp_plot:
                data_plot.append(temp_plot)
                temp_plot = []
            for year in range(2009, 2020, 1):
                count = int(data[(data.Light_Conditions == i) & (
                    data.Year == year)].count()['Road_Type'])
                temp_plot.append(count)

        x_label = [2009, 2010, 2011, 2012, 2013,
                   2014, 2015, 2016, 2017, 2018, 2019]
        for j in range(len(data_plot)):
            plt.plot(x_label, data_plot[j])

        plt.title("Trend of the different " + condition + " over the years")
        plt.ylabel("Number of occurance")
        plt.xlabel("Year")
        plt.legend(light_conditions, title='title',
                   bbox_to_anchor=(1.05, 1), loc='upper left')
        st.pyplot(plt)

    if condition == "Weather Condition":
        weather_conditions = ['Fog or mist',
                              'Snowing with high winds',
                              'Raining without high winds',
                              'Raining with high winds',
                              'Snowing without high winds',
                              'Fine without high winds',
                              'Fine with high winds',
                              'Unknown',
                              'Other']

        for i in weather_conditions:
            if temp_plot:
                data_plot.append(temp_plot)
                temp_plot = []
            for year in range(2009, 2020, 1):
                count = int(data[(data.Weather_Conditions == i) & (
                    data.Year == year)].count()['Weather_Conditions'])
                temp_plot.append(count)

        x_label = [2009, 2010, 2011, 2012, 2013,
                   2014, 2015, 2016, 2017, 2018, 2019]
        for j in range(len(data_plot)):
            plt.plot(x_label, data_plot[j])

        plt.title("Trend of the different " + condition + " over the years")
        plt.ylabel("Number of occurance")
        plt.xlabel("Year")
        plt.legend(weather_conditions, title='title',
                   bbox_to_anchor=(1.05, 1), loc='upper left')
        st.pyplot(plt)

    if condition == "Road Surface Condition":
        road_surface_conditions = ['Dry',
                                   'Snow',
                                   'Wet/Damp',
                                   'Frost/Ice',
                                   'Flood (Over 3cm of water)']

        for i in road_surface_conditions:
            if temp_plot:
                data_plot.append(temp_plot)
                temp_plot = []
            for year in range(2009, 2020, 1):
                count = int(data[(data.Road_Surface_Conditions == i) & (
                    data.Year == year)].count()['Weather_Conditions'])
                temp_plot.append(count)

        x_label = [2009, 2010, 2011, 2012, 2013,
                   2014, 2015, 2016, 2017, 2018, 2019]
        for j in range(len(data_plot)):
            plt.plot(x_label, data_plot[j])

        plt.title("Trend of the different " + condition + " over the years")
        plt.ylabel("Number of occurance")
        plt.xlabel("Year")
        plt.legend(road_surface_conditions, title='title',
                   bbox_to_anchor=(1.05, 1), loc='upper left')
        st.pyplot(plt)


def visualization(df):
    types = ["Accident Hotspots", "Plots"]
    plots = ["Scatter", "Categorical", "Trends"]
    choice = st.sidebar.selectbox("Types", types)

    path = checkPlatform()

    if choice == "Accident Hotspots":
        st.header("Accident Hotspots")
        st.write("""
        This map will plot out all the accidents that happened in the 
        selected date range. Due to data size rendering limitations,
        it can only render a month within a particular year.
        """)
        c1, c2 = st.beta_columns(2)

        year = c1.number_input(
            "Year", value=2009, min_value=2009, max_value=2019)
        month = c2.number_input("Month", value=1, min_value=1, max_value=12)

        if st.button("Launch Map"):
            pydeck(df, year, month)

    elif choice == "Plots":
        plot_choice = st.sidebar.selectbox("Plots", plots)

        if plot_choice == "Scatter":
            st.header("Scatter Plot Visualisation")
            st.write("""
                This section allows you to create your own customised scatter plot 
                data visualisation, by using the UK Traffic Accidents dataset. You 
                can also make a comparison between the different kinds of charts as well.
            """)

            with st.beta_expander("Pre-loaded Visualisation with Full Dataset"):
                image = Image.open(path + "scatter_severity.png")
                st.write(
                    "This figure shows the pre-generated dataset for all the " +
                    "accident severity for the year _2009-2019_.")
                st.image(image, caption="Accident Severity",
                         use_column_width=True)

            with st.beta_expander("Customized Visualisation"):

                palette = st.selectbox(
                    "Palette", ['Paired', 'Accent', 'Purples', 'colorblind'])
                s = st.slider("Size per data point", value=1,
                              min_value=1, max_value=200)
                year = st.number_input(
                    "Year of Data to be Generated", value=2009, min_value=2009, max_value=2019)
                type = st.selectbox("Data to be Plotted", [
                    'Accident_Severity',
                    'Road_Type',
                    'Light_Conditions',
                    'Weather_Conditions',
                    'Road_Surface_Conditions'])

                if st.button("Plot Customised Visualisation"):
                    scatterplot(df, palette, s, year, type)

            with st.beta_expander("Compare Your Charts"):
                with st.beta_container():
                    st.write(
                        "Generate 2 custom scatter plots side by side for comparison!")
                    c1, c2 = st.beta_columns((1, 1))

                    with c1:
                        palette1, s1, year1, type1 = scatter_form()

                    with c2:
                        palette2 = st.selectbox(
                            "Palette  ", ['Paired', 'Accent', 'Purples', 'colorblind'])
                        s2 = st.slider("Size per data point  ",
                                       value=1, min_value=1, max_value=200)
                        year2 = st.number_input(
                            "Year of data to be generated  ", value=2009, min_value=2009, max_value=2019)
                        type2 = st.selectbox("Data to be plotted  ", [
                                             'Accident_Severity', 'Road_Type', 'Light_Conditions', 'Weather_Conditions', 'Road_Surface_Conditions'])

                    if st.button("Launch Comparison"):
                        c3, c4 = st.beta_columns((1, 1))
                        with c3:
                            scatterplot(df, palette1, s1, year1, type1)
                        with c4:
                            c4 = scatterplot(df, palette2, s2, year2, type2)

        elif plot_choice == "Categorical":
            st.header("Categorical Visualisation")
            st.write("""
                This section is to show the breakdown of accidents and its severity 
                according to the conditions selected. You can use this visualisation data 
                to help you have an insight on what kind of conditions that are present, 
                which may contribute to traffic accidents.""")
            with st.beta_expander("Customized Visualisation"):

                palette = st.selectbox(
                    "Palette", ['Paired', 'Accent', 'Purples', 'colorblind'])
                hue = st.selectbox("Condition", [
                                   'Road_Type', 'Light_Conditions', 'Weather_Conditions', 'Road_Surface_Conditions'])
                year = st.selectbox(
                    "Year", [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019])
                if st.button("Plot Visualisation"):
                    categoricalplot(df.query('Year==@year'), hue, palette)

        elif plot_choice == "Trends":
            st.header("Line Chart Visualisation")
            st.write(
                "View the trends of the different conditions that are present in road accidents over the years.")

            condition = st.selectbox("Select the Road Condition to plot", [
                                     'Road Type', 'Light Condition', 'Weather Condition', 'Road Surface Condition'])

            lineplot(df, condition)
