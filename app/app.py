import streamlit as st
import numpy as np
import pandas as pd
import streamlit.components.v1 as components
import matplotlib.pyplot as plt
import os

from home import home
from visualization import visualization
from prediction import prediction
import eda


from PIL import Image

st.set_page_config(page_title='TrafStats', page_icon=None,
                   layout='centered', initial_sidebar_state='auto')


def checkPlatform():  # Check if running on local or heroku
    if(os.path.exists(os.path.join(os.getcwd(), "data"))):
        path = "data/"
    else:
        path = "app/data/"

    return path


@st.cache(allow_output_mutation=True)
def load_data():
    # get os path
    path = checkPlatform()

    accidents_df = pd.read_csv(path + 'accidents_dv.dat')
    return accidents_df


title_temp = """
	<div style="background-color:#464e5f;padding:10px;border-radius:10px;margin:10px;">
	<h4 style="color:white;text-align:center;">{}</h1>
	<img src="data/logo.png" style="vertical-align: middle;float:left;width: 200px;height: 200px;border-radius: 50%;" >
	<h6>Author:{}</h6>
	<br/>
	<br/>	
	<p style="text-align:justify">{}</p>
	</div>
	"""

social_media_link = """
    <div style="display:flex;flex-wrap:wrap"><a href="https://www.facebook.com/TrafStats-116973923669056" target="_blank" rel="noopener noreferrer" style="text-decoration:none;border:0;width:36px;height:36px;padding:2px;margin:5px;color:#11CBE9;border-radius:50%;background-color:#0d2744;"><svg class="niftybutton-facebook" style="display:block;fill:currentColor" data-tag="fac" data-name="Facebook" viewBox="0 0 512 512" preserveAspectRatio="xMidYMid meet">
    <path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"></path>
    </svg></a><span style="padding:10px 0px;">Facebook</span></div>
"""

youtube_link = """
<p><a href="https://benjaminlw1bl.wixsite.com/my-site" style="padding:0px 10px">Our Website</a></p>
<p><a href="https://www.youtube.com/channel/UCJtanS20sct323RZHbysI3Q" style="padding:0px 10px">YouTube</a></p>
"""

path = checkPlatform()
logo = Image.open(path + "logo.png")

h1, h2 = st.beta_columns([1, 8])
with h1:
    st.image(logo, use_column_width=True)
with h2:
    h2 = st.markdown("""# TrafStats""")
st.write("""_Providing you traffic statistics in the United Kingdom_
***
""")

df = load_data()

menu = ["Home", "Visualisation", "Predictions", "Explore Dataset"]
st.sidebar.header("Navigation Panel")
choice = st.sidebar.selectbox("Menu", menu)

if choice == "Home":
    home(df)

elif choice == "Explore Dataset":
    eda.eda(df)

elif choice == "Visualisation":
    visualization(df)

elif choice == "Predictions":
    prediction()

st.sidebar.subheader("ABOUT")
st.sidebar.info(
    """
        This application is designed to provide analysis on
        the traffic statistics in the United Kingdom.
        All data scientists around the world are welcomed
        to use TrafStats to perform your analysis work.
        ***
        Done by UOW SIM Final Year Students (TrafStats. 2021)
    """)

st.sidebar.markdown(social_media_link, unsafe_allow_html=True)
st.sidebar.markdown(youtube_link, unsafe_allow_html=True)