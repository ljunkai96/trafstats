import streamlit as st
import pandas as pd
import base64
import os


def checkPlatform():  # Check if running on local or heroku
    if(os.path.exists(os.path.join(os.getcwd(), "data"))):
        path = "data/"
    else:
        path = "app/data/"

    return path


def downloadcsv(df):
    csv = df.to_csv(index=False)
    # strings <-> bytes conversion
    b64 = base64.b64encode(csv.encode()).decode()
    href = f'<a href="data:file/csv;base64,{b64}" download = "accidents.csv">Download CSV file</a>'
    return href


def eda(df):

    st.header("Explore Dataset")
    st.write(
        "Feel free to explore and create your own unique dataset for your data analysis!")

    # Details of original full dataset
    st.subheader("Overview of the UK Traffic Accidents (2009 - 2019) Dataset")
    st.markdown("### Summary")
    st.write(df.describe().T)
    st.markdown("### Data Types")
    st.write(df.dtypes)
    st.write("Rows and Columns: ", df.shape)
    st.write("### Top 5 Rows")
    st.write(df.head())
    st.write("***")

    # Customizing
    st.subheader("Customize Your Own Dataset Here")
    c1, c2 = st.beta_columns((1, 1))

    all_columns = df.columns.tolist()
    selected_columns = st.multiselect(
        'Select Columns', all_columns, default=all_columns)

    with c2:
        with st.beta_expander("Add Filtering"):
            road_type = ['All',
                         'Dual carriageway',
                         'One way street',
                         'Slip road',
                         'Roundabout',
                         'Unknown',
                         'Single carriageway']
            light_conditions = ['All',
                                'Darkness: No street lighting',
                                'Daylight: Street light present',
                                'Darkness: No street lighting',
                                'Darkness: Street lights present and lit',
                                'Darkness: Street lights present but unlit',
                                'Darkness: Street lighting unknown']
            weather_conditions = ['All',
                                  'Fog or mist',
                                  'Snowing with high winds',
                                  'Raining without high winds',
                                  'Raining with high winds',
                                  'Snowing without high winds',
                                  'Fine without high winds',
                                  'Fine with high winds'
                                  'Unknown'
                                  'Other']

            road_surface_conditions = ['All',
                                       'Dry',
                                       'Snow',
                                       'Wet/Damp',
                                       'Frost/Ice',
                                       'Flood (Over 3cm of water)']
            year = st.selectbox(
                "Year", ['All', 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019])
            road = st.selectbox("Road Type", road_type)
            light = st.selectbox("Light Condition", light_conditions)
            weather = st.selectbox("Weather Condition", weather_conditions)
            road_surface = st.selectbox(
                "Road Surface Condition", road_surface_conditions)

    # Process custom df
    new_df = df[selected_columns]
    if year != 'All':
        new_df = new_df.query('Year==@year')
    if road != 'All':
        new_df = new_df.query('Road_Type==@road')
    if light != 'All':
        new_df = new_df.query('Light_Conditions==@light')
    if weather != 'All':
        new_df = new_df.query('Weather_Conditions==@weather')
    if road_surface != 'All':
        new_df = new_df.query('Road_Surface_Conditions==@road_surface')

    with c1:
        st.write("### Basic Criterias")
        begin = st.number_input("Row to Begin", value=0,
                                min_value=0, max_value=len(new_df))
        end = st.number_input("Row to End", value=begin,
                              min_value=begin, max_value=len(new_df))

    new_df = new_df[begin:end]
    st.markdown("""
    ### Your Customised Dataset
    """)
    if end-begin == 0:
        st.markdown("* You have not selected any number of rows to view")

    elif end-begin <= 50000:
        st.dataframe(new_df)

    else:
        st.warning("Range of rows must be < 50000 due to server limitations.")

    # Download feature
    st.write("Download your customized dataset here:")
    mem = int(new_df.memory_usage(index=True).sum() / 1000000)
    fiftymb = int(40000000 / 1000000)
    st.write("Max Memory(mb):", fiftymb, "Dataset Memory Usage(mb): ", mem)
    if st.button("Generate Download File"):
        if fiftymb > mem:
            st.markdown(downloadcsv(new_df), unsafe_allow_html=True)
        else:
            st.warning(
                "Customized dataset exceeds 40mb limit. " +
                "Please reduce the number of rows or " +
                "the selected column to enable downloading")
